# Chameleon Admin - Modern Bootstrap 4 WebApp & Dashboard HTML Template + UI Kit

V1.2 : [22/03/2019]
[Added]
- New Bootstrap components toaster, spinner &amp; custom switch
[Updated]
- Updated template to latest bootstrap v4.3.1
[Fixed]
- tinyMCE Editor issue resolved
- Menu collapse popout position issue resolved
- Main Menu scroll issue resolved for medium screen


V1.1 : [03/12/2018]
[Updated]
- Updated theme to latest bootstrap v4.3.1
[Fixed]
- Fixed horizontal layout menu issue for small screen
- Small issues and bug fix


V1.0 : [13/07/2018] 
- Initial Release
